using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using AspNetCoreRateLimit;
using backend.DataAccess.FmpCloudApi;
using backend.DataAccess.FmpCloudApi.Interface;
using backend.DataTransfer;
using backend.Service;
using backend.Service.Interface;
using data_access;
using data_access.Model;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace backend
{
    public class Startup
    {
        private const string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddMemoryCache();

            var connectionString = Configuration.GetConnectionString("OpenStockSimDatabase");
            var dbPassword = Configuration["DbPassword"];
            connectionString = $"{connectionString}Password={dbPassword};";
            services.AddDbContext<OpenStockSimDbContext>(options => options.UseNpgsql(connectionString));

            services.AddDefaultIdentity<OpenStockSimUser>()
                .AddEntityFrameworkStores<OpenStockSimDbContext>();

            services.Configure<IdentityOptions>(options => {
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 8;
                options.Password.RequiredUniqueChars = 1;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                options.User.RequireUniqueEmail = true;
            });

            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.WithOrigins("http://localhost:3000", "http://localhost:4000")
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                // TODO: Put in configuration file
                options.ClaimsIssuer = Constants.JwtIssuer;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = Constants.JwtIssuer,
                    ValidateAudience = true,
                    ValidAudience = Constants.JwtAudience,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = Constants.SigningKey,
                    RequireExpirationTime = false,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };
                options.SaveToken = true;
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(Constants.ApiUserPolicy, policy => policy.RequireClaim(Constants.JwtRol, Constants.JwtApiAccess));
            });

            services.Configure<IpRateLimitOptions>(Configuration.GetSection("IpRateLimiting"));
            services.Configure<IpRateLimitPolicies>(Configuration.GetSection("IpRateLimitPolicies"));
            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();

            var sendGridApiKey = Configuration["SendGridApiKey"];
            var websiteUrl = Configuration["WebsiteUrl"];
            var fmpCloudApiKey = Configuration["FmpCloudApiKey"];

            services.AddHttpClient();
            services.AddHttpClient(Constants.FmpCloudHttpClient);

            services.AddTransient<JwtSecurityTokenHandler>();
            services.AddTransient<IAuthTokenService, AuthTokenService>();
            services.AddTransient<IEmailService, EmailService>(x => new EmailService(sendGridApiKey, websiteUrl));
            services.AddTransient<IFmpCloudApiQuoteDao, FmpCloudApiQuoteDao>(x => new FmpCloudApiQuoteDao(fmpCloudApiKey, x.GetService<IHttpClientFactory>(), x.GetService<ILogger<FmpCloudApiQuoteDao>>()));

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, OpenStockSimDbContext openStockSimDbContext)
        {
            openStockSimDbContext.Database.Migrate();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseIpRateLimiting();

            app.UseCors(MyAllowSpecificOrigins);

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
