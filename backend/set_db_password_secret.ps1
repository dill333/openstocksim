$postgresFolder = "$((Get-ChildItem Env:APPDATA).Value)\openstocksim-postgres\";
$pgpassword = Get-Content "$postgresFolder\password";
dotnet user-secrets set "DbPassword" $pgpassword