using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Threading.Tasks;
using backend.DataAccess;
using backend.DataTransfer;
using backend.Model.Authentication;
using backend.Service.Interface;
using data_access;
using data_access.Model;
using Microsoft.AspNetCore.Identity;

namespace backend.Service
{
    public class AuthTokenService : IAuthTokenService
    {
        private readonly JwtSecurityTokenHandler _jwtSecurityTokenHandler;
        private readonly OpenStockSimDbContext _openStockSimDbContext;
        private readonly UserManager<OpenStockSimUser> _userManager;

        public AuthTokenService(JwtSecurityTokenHandler jwtSecurityTokenHandler, OpenStockSimDbContext openStockSimDbContext, UserManager<OpenStockSimUser> userManager)
        {
            _jwtSecurityTokenHandler = jwtSecurityTokenHandler;
            _openStockSimDbContext = openStockSimDbContext;
            _userManager = userManager;
        }

        public async Task<AuthToken> GenerateAuthToken(OpenStockSimUser user, string ipAddress)
        {
            var identity = GenerateClaimsIdentity(user.UserName, user.Id);
            var issueTime = DateTime.UtcNow;
            var expireTimeSpan = TimeSpan.FromMinutes(120);
            var expireTime = issueTime + expireTimeSpan;
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, ((long)Math.Round((issueTime - DateTime.UnixEpoch).TotalSeconds)).ToString(), ClaimValueTypes.Integer64),
                identity.FindFirst(Constants.JwtRol),
                identity.FindFirst(Constants.JwtId)
            };

            var jwt = new JwtSecurityToken(Constants.JwtIssuer, Constants.JwtAudience, claims, issueTime, expireTime, Constants.SigningCredentials);

            var encodedJwt = _jwtSecurityTokenHandler.WriteToken(jwt);

            var refreshExpireTimeSpan = TimeSpan.FromDays(1);
            var refreshToken = await GenerateRefreshToken(user.Id, ipAddress, refreshExpireTimeSpan);

            return new AuthToken
            {
                Id = identity.Claims.Single(x => x.Type == Constants.JwtId).Value,
                AccessToken = encodedJwt,
                AccessExpiresIn = (int)expireTimeSpan.TotalSeconds,
                RefreshToken = refreshToken,
                RefreshExpiresIn = (int)refreshExpireTimeSpan.TotalSeconds
            };
        }

        public async Task<AuthToken> ValidateRefreshToken(string token, string ipAddress)
        {
            var refreshToken = await _openStockSimDbContext.RefreshTokens.FindAsync(token);
            if(refreshToken != null && refreshToken.IpAddress == ipAddress && refreshToken.ExpireDateTime >= DateTime.UtcNow && !refreshToken.Revoked)
            {
                // Valid, revoke this and grant a new token
                refreshToken.Revoked = true;
                _openStockSimDbContext.Update(refreshToken);
                await _openStockSimDbContext.SaveChangesAsync();

                var user = await _userManager.FindByIdAsync(refreshToken.UserId);
                if(user == null)
                {
                    // Something is seriously wrong
                    return null;
                }
                return await GenerateAuthToken(user, refreshToken.IpAddress);
            }
            else
            {
                return null;
            }
        }

        private ClaimsIdentity GenerateClaimsIdentity(string username, string id)
        {
            return new ClaimsIdentity(new GenericIdentity(username, "Token"), new[]
            {
                new Claim(Constants.JwtId, id),
                new Claim(Constants.JwtRol, Constants.JwtApiAccess)
            });
        }

        private async Task<string> GenerateRefreshToken(string userId, string ipAddress, TimeSpan expireTimeSpan)
        {
            string token;
            using(var rng = RandomNumberGenerator.Create())
            {
                var randomBytes = new byte[32];
                rng.GetBytes(randomBytes);
                token = Convert.ToBase64String(randomBytes);
            }

            var refreshToken = new RefreshToken
            {
                Token = token,
                UserId = userId,
                ExpireDateTime = DateTime.UtcNow + expireTimeSpan,
                Revoked = false,
                IpAddress = ipAddress
            };

            _openStockSimDbContext.Add(refreshToken);
            await _openStockSimDbContext.SaveChangesAsync();

            return token;
        }
    }
}