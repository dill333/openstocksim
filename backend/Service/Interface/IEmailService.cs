using System.Threading.Tasks;

namespace backend.Service.Interface
{
    public interface IEmailService
    {
        Task SendConfirmationEmail(string emailAddress, string confirmToken);
        Task SendResetPasswordEmail(string emailAddress, string passwordResetToken);
    }
}