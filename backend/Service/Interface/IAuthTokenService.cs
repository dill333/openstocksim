using System.Threading.Tasks;
using backend.Model.Authentication;
using data_access.Model;

namespace backend.Service.Interface
{
    public interface IAuthTokenService
    {
        Task<AuthToken> GenerateAuthToken(OpenStockSimUser user, string ipAddress);
        Task<AuthToken> ValidateRefreshToken(string token, string ipAddress);
    }
}