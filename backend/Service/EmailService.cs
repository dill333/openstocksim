using System;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using backend.Service.Interface;
using Microsoft.Extensions.Logging;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace backend.Service
{
    public class EmailService : IEmailService
    {
        private readonly string _sendGridApiKey;
        private readonly string _websiteUrl;

        public EmailService(string sendGridApiKey, string websiteUrl)
        {
            _sendGridApiKey = sendGridApiKey;
            _websiteUrl = websiteUrl;
        }

        public async Task SendConfirmationEmail(string emailAddress, string confirmToken)
        {
            const string subject = "Confirm your email with openstocksim.com";
            var confirmUriBuilder = new UriBuilder(_websiteUrl);
            confirmUriBuilder.Path = "/confirm-email";
            var parameters = HttpUtility.ParseQueryString(string.Empty);
            parameters.Add("email", emailAddress);
            parameters.Add("confirmToken", confirmToken);
            confirmUriBuilder.Query = parameters.ToString();
            var message = $"Please click here to confirm your email address: {confirmUriBuilder.Uri}";
            await SendEmail(emailAddress, subject, message);
        }

        public async Task SendResetPasswordEmail(string emailAddress, string passwordResetToken)
        {
            const string subject = "Reset your password with openstocksim.com";
            var resetPasswordUriBuilder = new UriBuilder(_websiteUrl);
            resetPasswordUriBuilder.Path = "/reset-password";
            var parameters = HttpUtility.ParseQueryString(string.Empty);
            parameters.Add("email", emailAddress);
            parameters.Add("passwordResetToken", passwordResetToken);
            resetPasswordUriBuilder.Query = parameters.ToString();
            var message = $"Please click here to reset your password: {resetPasswordUriBuilder.Uri}";
            await SendEmail(emailAddress, subject, message);
        }

        private async Task SendEmail(string to, string subject, string message)
        {
            var client = new SendGridClient(_sendGridApiKey);
            var from = new EmailAddress("no-reply@openstocksim.com", "OpenStockSim");
            var toEmail = new EmailAddress(to);
            var email = MailHelper.CreateSingleEmail(from, toEmail, subject, message, message);
            var response = await client.SendEmailAsync(email);
            if(response.StatusCode != HttpStatusCode.Accepted)
            {
                // TODO: Throw an exception or something
            }
        }
    }
}