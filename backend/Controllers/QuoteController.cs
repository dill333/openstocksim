using System;
using System.Threading.Tasks;
using backend.DataAccess.FmpCloudApi.Interface;
using backend.DataTransfer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class QuoteController : ControllerBase
    {
        private readonly IFmpCloudApiQuoteDao _fmpCloudApiQuoteDao;

        public QuoteController(IFmpCloudApiQuoteDao fmpCloudApiQuoteDao)
        {
            _fmpCloudApiQuoteDao = fmpCloudApiQuoteDao;
        }

        [HttpGet]
        [Authorize(Policy = Constants.ApiUserPolicy)]
        public async Task<IActionResult> GetQuote(string symbol)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(string.IsNullOrEmpty(symbol))
            {
                return BadRequest("Invalid symbol");
            }

            var result = await _fmpCloudApiQuoteDao.GetRealTimeQuote(symbol);

            return new OkObjectResult(result);
        }
    }
}