using System;
using System.Threading.Tasks;
using backend.DataTransfer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    [Authorize(Policy = Constants.ApiUserPolicy)]
    public class OrderController : ControllerBase
    {
        [HttpPost]
        public async Task PlaceOrder()
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public async Task CancelOrder()
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public async Task GetOrderDetails()
        {
            throw new NotImplementedException();
        }
    }
}