using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using backend.DataTransfer;
using backend.Extensions;
using backend.Model.Authentication;
using backend.Service.Interface;
using data_access.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace backend.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly UserManager<OpenStockSimUser> _userManager;
        private readonly IAuthTokenService _authTokenService;

        public AuthenticationController(UserManager<OpenStockSimUser> userManager, IAuthTokenService authTokenService)
        {
            _userManager = userManager;
            _authTokenService = authTokenService;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _userManager.FindByEmailOrNameAsync(model.UsernameOrEmail);
            if(user == null || !await _userManager.CheckPasswordAsync(user, model.Password))
            {
                return new BadRequestObjectResult(new LoginErrorModel
                {
                    ErrorEnum = LoginErrorEnum.InvalidCredentials,
                    ErrorMessage = "Invalid username/email or password"
                });
            }

            if(!user.EmailConfirmed)
            {
                return new BadRequestObjectResult(new LoginErrorModel
                {
                    ErrorEnum = LoginErrorEnum.EmailNotConfirmed,
                    ErrorMessage = "Email has not been confirmed"
                });
            }

            var token = await _authTokenService.GenerateAuthToken(user, Request.HttpContext.Connection.RemoteIpAddress.ToString());
            var jsonToken = JsonConvert.SerializeObject(token);
            return new OkObjectResult(jsonToken);
        }

        [HttpPost]
        public async Task<IActionResult> Refresh([FromBody]RefreshTokenViewModel refreshToken)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var token = await _authTokenService.ValidateRefreshToken(refreshToken.RefreshToken, Request.HttpContext.Connection.RemoteIpAddress.ToString());
            if(token == null)
            {
                return new BadRequestResult();
            }
            var jsonToken = JsonConvert.SerializeObject(token);
            return new OkObjectResult(jsonToken);
        }
    }
}