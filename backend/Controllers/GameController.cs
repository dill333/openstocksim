using System;
using System.Threading.Tasks;
using backend.DataTransfer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    [Authorize(Policy = Constants.ApiUserPolicy)]
    public class GameController : ControllerBase
    {
        [HttpPost]
        public async Task CreateGame()
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public async Task GetGameDetails()
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public async Task EndGame()
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public async Task JoinGame()
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public async Task LeaveGame()
        {
            throw new NotImplementedException();
        }

        [HttpPost]
        public async Task InviteUser()
        {
            throw new NotImplementedException();
        }
    }
}