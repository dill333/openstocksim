using System.Linq;
using System.Threading.Tasks;
using backend.DataTransfer;
using backend.Extensions;
using backend.Model;
using backend.Model.Account;
using backend.Service.Interface;
using data_access.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<OpenStockSimUser> _userManager;
        private readonly IEmailService _emailService;

        public AccountController(UserManager<OpenStockSimUser> userManager, IEmailService emailService)
        {
            _userManager = userManager;
            _emailService = emailService;
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegistrationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new OpenStockSimUser
            {
                Email = model.Email,
                UserName = model.Username
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return new BadRequestObjectResult(result);
            }

            return new OkObjectResult("Account created");
        }

        [HttpGet]
        [Authorize(Policy = Constants.ApiUserPolicy)]
        public async Task<IActionResult> AccountDetails()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var id = HttpContext?.User?.Claims?.FirstOrDefault(x => x.Type == Constants.JwtId)?.Value;
            var user = await _userManager.FindByIdAsync(id);

            AccountInfoModel accountInfo = null;
            if (user != null)
            {
                accountInfo = new AccountInfoModel
                {
                    Username = user.UserName,
                    Email = user.Email,
                    EmailConfirmed = user.EmailConfirmed
                };
            }

            return new OkObjectResult(accountInfo);
        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword([FromBody]EmailViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (string.IsNullOrEmpty(model.Email))
            {
                return BadRequest("Invalid email");
            }

            var userToReset = await _userManager.FindByEmailAsync(model.Email);

            if (userToReset != null)
            {
                var resetToken = await _userManager.GeneratePasswordResetTokenAsync(userToReset);
                await _emailService.SendResetPasswordEmail(model.Email, resetToken);
            }

            return new OkResult();
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword([FromBody]ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (string.IsNullOrEmpty(model.Email))
            {
                return BadRequest("Invalid email");
            }

            var userToReset = await _userManager.FindByEmailAsync(model.Email);

            if (userToReset != null)
            {
                var result = await _userManager.ResetPasswordAsync(userToReset, model.ResetToken, model.NewPassword);
                if (!result.Succeeded)
                {
                    return new BadRequestObjectResult(result);
                }
            }

            return new OkResult();
        }

        [HttpPost]
        public async Task<IActionResult> SendConfirmationEmail([FromBody]EmailViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (string.IsNullOrEmpty(model.Email))
            {
                return BadRequest("Invalid email");
            }

            var userToConfirm = await _userManager.FindByEmailAsync(model.Email);

            if (userToConfirm != null && !userToConfirm.EmailConfirmed)
            {
                var token = await _userManager.GenerateEmailConfirmationTokenAsync(userToConfirm);
                await _emailService.SendConfirmationEmail(userToConfirm.Email, token);
            }

            return new OkResult();
        }

        [HttpPost]
        public async Task<IActionResult> ConfirmEmail([FromBody]ConfirmEmailViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (string.IsNullOrEmpty(model.Email))
            {
                return BadRequest("Invalid email");
            }

            var userToConfirm = await _userManager.FindByEmailAsync(model.Email);

            if (userToConfirm != null)
            {
                var result = await _userManager.ConfirmEmailAsync(userToConfirm, model.ConfirmToken);
                if (!result.Succeeded)
                {
                    return new BadRequestObjectResult(result);
                }
            }

            return new OkResult();
        }
    }
}