#!/bin/bash

PGPASSWORD=`cat /var/lib/openstocksim-postgres/password`
dotnet user-secrets set "DbPassword" $PGPASSWORD
