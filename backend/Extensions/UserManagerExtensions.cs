using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace backend.Extensions
{
    public static class UserManagerExtensions
    {
        public static async Task<TUser> FindByEmailOrNameAsync<TUser>(this UserManager<TUser> userManager, string usernameOrEmail) where TUser : class
        {
            if(string.IsNullOrEmpty(usernameOrEmail))
            {
                return await Task.FromResult<TUser>(null);
            }

            TUser user;
            var userByEmail = await userManager.FindByEmailAsync(usernameOrEmail);
            if(userByEmail != null)
            {
                user = userByEmail;
            }
            else
            {
                user = await userManager.FindByNameAsync(usernameOrEmail);
            }
            return user;
        }
    }
}