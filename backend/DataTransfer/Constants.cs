using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace backend.DataTransfer
{
    public class Constants
    {
        public const string JwtRol = "rol";
        public const string JwtId = "id";
        public const string JwtIssuer = "OpenStockSimApi";
        public const string JwtAudience = "https://localhost:5001";
        private const string SecretKey = "idkifthiswillactuallyworkbutwewillSEE";
        public static SymmetricSecurityKey SigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecretKey));
        public static SigningCredentials SigningCredentials = new SigningCredentials(SigningKey, SecurityAlgorithms.HmacSha256);
        public const string JwtApiAccess = "api_access";
        public const string ApiUserPolicy = "ApiUser";
        public const string FmpCloudHttpClient = "FmpCloudHttpClient";
    }
}