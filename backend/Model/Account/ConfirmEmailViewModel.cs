namespace backend.Model.Account
{
    public class ConfirmEmailViewModel
    {
        public string Email { get; set; }
        public string ConfirmToken { get; set; }
    }
}