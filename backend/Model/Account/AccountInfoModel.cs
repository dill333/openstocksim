namespace backend.Model.Account
{
    public class AccountInfoModel
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
    }
}