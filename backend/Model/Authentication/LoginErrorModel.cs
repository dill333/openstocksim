namespace backend.Model.Authentication
{
    public class LoginErrorModel
    {
        public LoginErrorEnum ErrorEnum { get; set; }
        public string ErrorMessage { get; set; }
    }
}