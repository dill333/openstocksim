namespace backend.Model.Authentication
{
    public class RefreshTokenViewModel
    {
        public string RefreshToken { get; set; }
    }
}