namespace backend.Model.Authentication
{
    public enum LoginErrorEnum
    {
        InvalidCredentials = 0,
        EmailNotConfirmed = 1
    }
}