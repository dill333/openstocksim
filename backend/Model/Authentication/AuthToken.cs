namespace backend.Model.Authentication
{
    public class AuthToken
    {
        public string Id { get; set; }
        public string AccessToken { get; set; }
        public int AccessExpiresIn { get; set; }
        public string RefreshToken { get; set; }
        public int RefreshExpiresIn { get; set; }
    }
}