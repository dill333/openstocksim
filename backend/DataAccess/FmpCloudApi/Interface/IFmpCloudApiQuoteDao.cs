using System.Threading.Tasks;
using backend.DataTransfer.FmpCloudApi;

namespace backend.DataAccess.FmpCloudApi.Interface
{
    public interface IFmpCloudApiQuoteDao
    {
        Task<FmpCloudApiQuoteDto> GetRealTimeQuote(string symbol);
    }
}