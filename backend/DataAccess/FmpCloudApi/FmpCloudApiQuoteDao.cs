using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using backend.DataAccess.FmpCloudApi.Interface;
using backend.DataTransfer;
using backend.DataTransfer.FmpCloudApi;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace backend.DataAccess.FmpCloudApi
{
    public class FmpCloudApiQuoteDao : IFmpCloudApiQuoteDao
    {
        private readonly string _apiKey;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<FmpCloudApiQuoteDao> _logger;

        private const string QuoteUrl = "https://fmpcloud.io/api/v3/quote/{0}?apikey={1}";

        public FmpCloudApiQuoteDao(string apiKey, IHttpClientFactory httpClientFactory, ILogger<FmpCloudApiQuoteDao> logger)
        {
            _apiKey = apiKey;
            _httpClientFactory = httpClientFactory;
            _logger = logger;
        }

        public async Task<FmpCloudApiQuoteDto> GetRealTimeQuote(string symbol)
        {
            var url = string.Format(QuoteUrl, symbol, _apiKey);
            var httpClient = _httpClientFactory.CreateClient(Constants.FmpCloudHttpClient);
            var response = await httpClient.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();
            if(!response.IsSuccessStatusCode)
            {
                _logger.LogError($"Quote API returned status code {response.StatusCode}, with content {content}");
                throw new InvalidOperationException($"Received status code {response.StatusCode} from quote API!");
            }

            var results = JsonConvert.DeserializeObject<IEnumerable<FmpCloudApiQuoteDto>>(content);
            if(results != null)
            {
                return results.FirstOrDefault();
            }
            return null;
        }
    }
}