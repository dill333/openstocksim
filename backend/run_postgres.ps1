$postgresFolder = "$((Get-ChildItem Env:APPDATA).Value)\openstocksim-postgres\";

docker run --name openstocksim-postgres -p 5432:5432 `
    -v $PWD\openstocksim-postgres.conf:/etc/postgresql/postgresql.conf `
    -v ${postgresFolder}:/var/lib/postgresql/ `
    -e POSTGRES_PASSWORD_FILE=/var/lib/postgresql/password `
    -d postgres:alpine
