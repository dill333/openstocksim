export interface AuthToken {
  Id: string;
  AccessToken: string;
  AccessExpiresIn: number;
  RefreshToken: string;
  RefreshExpiresIn: number;
}
