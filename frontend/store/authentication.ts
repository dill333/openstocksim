import { AxiosError } from "axios";
import { AuthToken } from "../model/AuthToken";

export const state = () => ({
  token: {} as AuthToken,
  status: "",
  errorMessage: "",
  errorEnum: -1
});

export const getters = {
  isAuthenticated: (authState: any) => !!authState.token.AccessToken,
  authStatus: (authState: any) => authState.status,
  authToken: (authState: any) => authState.token.AccessToken,
  authErrorMessage: (authState: any) => authState.errorMessage,
  authErrorEnum: (authState: any) => authState.errorEnum,
  refreshToken: (authState: any) => authState.token.RefreshToken
};

export const mutations = {
  authRequest: (authState: any) => {
    authState.status = "Attempting authentication request";
    authState.errorMessage = "";
    authState.errorEnum = -1;
  },
  authSuccess: (authState: any, authToken: AuthToken) => {
    authState.status = "Authentication succeeded";
    authState.token = authToken;
    authState.errorMessage = "";
    authState.errorEnum = -1;
  },
  authError: (authState: any, error: any) => {
    authState.status = "Error";
    authState.errorMessage = error.errorMessage;
    authState.errorEnum = error.errorEnum;
  },
  authLogout: (authState: any) => {
    authState.token = {} as AuthToken;
    authState.status = "";
    authState.errorMessage = "";
    authState.errorEnum = -1;
  }
};

export const actions = {
  authRequest: async function(
    { commit, dispatch }: { commit: any; dispatch: any },
    credentials: any
  ) {
    // This is stupid, it's supposed to work but it seems like @nuxtjs/axios isn't imported properly in the tsconfig.json file
    await (this as any).$axios
      .$post("/authentication/login", credentials)
      .then(function(response: any) {
        commit("authSuccess", response);
      })
      .catch(function(error: AxiosError) {
        commit("authError", error?.response?.data ?? "Unknown error");
      });
  },
  authLogout: ({ commit, dispatch }: { commit: any; dispatch: any }) => {
    commit("authLogout");
  }
};
