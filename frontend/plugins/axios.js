export default function({ $axios, app, store }) {
  $axios.onRequest(request => {
    const token = store.getters["authentication/authToken"];
    if (token) {
      request.headers["Authorization"] = "Bearer " + token;
    }
    return request;
  });

  $axios.onError(error => {
    const originalRequest = error.config;
    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;
      const refreshToken = store.getters["authentication/refreshToken"];
      if (refreshToken) {
        return $axios
          .$post("/authentication/refresh", { RefreshToken: refreshToken })
          .then(result => {
            if (result !== null) {
              store.commit("authentication/authSuccess", result);
              originalRequest.headers["Authorization"] =
                "Bearer " + result.AccessToken;
              return $axios(originalRequest);
            }
          })
          .catch(() => {
            store.commit("authentication/authLogout");
          });
      }
    }
  });
}
