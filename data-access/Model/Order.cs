using System;
using System.ComponentModel.DataAnnotations;

namespace data_access.Model
{
    public class Order
    {
        [Key]
        public Guid Id { get; set; }
        public Guid GameId { get; set; }
        public string UserId { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime? ExpirationDateTime { get; set; }
        public OrderTypeEnum OrderType { get; set; }
        public string StockSymbol { get; set; }
        public string Exchange { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public bool Fulfilled { get; set; }
        public bool Cancelled { get; set; }
    }
}