using System;
using System.ComponentModel.DataAnnotations;

namespace data_access.Model
{
    public class UserStock
    {
        [Key]
        public Guid Id { get; set; }
        public Guid UserGameProfileId { get; set; }
        public string StockSymbol { get; set; }
        public int Quantity { get; set; }
    }
}