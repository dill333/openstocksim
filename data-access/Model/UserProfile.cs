using System;
using System.ComponentModel.DataAnnotations;

namespace data_access.Model
{
    public class UserProfile
    {
        [Key]
        public Guid Id { get; set; }
        public Guid GameId { get; set; }
        public string UserId { get; set; }
        public DateTime JoinDateTime { get; set; }
        public bool Active { get; set; }
        public decimal CurrentMoney { get; set; }
    }
}