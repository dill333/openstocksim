using System;
using System.ComponentModel.DataAnnotations;

namespace data_access.Model
{
    public class Transaction
    {
        [Key]
        public Guid Id { get; set; }
        public Guid GameId { get; set; }
        public string UserId { get; set; }
        public Guid OrderId { get; set; }
        public DateTime ExecutedDateTime { get; set; }
        public string StockSymbol { get; set; }
        public string Exchange { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
    }
}