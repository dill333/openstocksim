using System;
using System.ComponentModel.DataAnnotations;

namespace data_access.Model
{
    public class Game
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal StartingMoney { get; set; }
        public bool InviteOnly { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string AuthorId { get; set; }
        public bool Active { get; set; }
    }
}