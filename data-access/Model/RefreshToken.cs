using System;
using System.ComponentModel.DataAnnotations;

namespace data_access.Model
{
    public class RefreshToken
    {
        [Key]
        public string Token { get; set; }
        public string UserId { get; set; }
        public DateTime ExpireDateTime { get; set; }
        public bool Revoked { get; set; }
        public string IpAddress { get; set; }
    }
}