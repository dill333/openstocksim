using System.Diagnostics.CodeAnalysis;
using data_access.Model;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace data_access
{
    public class OpenStockSimDbContext : IdentityDbContext<OpenStockSimUser>
    {
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<UserStock> UserStocks { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        public OpenStockSimDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }
    }
}